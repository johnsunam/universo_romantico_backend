import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Injectable()

export class ValidationService{
    pageForm: any;
    formErrors: object;
    validationMessages: object;

    constructor(private fb: FormBuilder)
    {

    }

    buildForm(field: any): void{
        console.log(field);
        this.pageForm = this.fb.group(field);
        console.log(this.pageForm);
        this.pageForm.valueChanges.subscribe(data => this.onValueChanged(data));
        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.pageForm) { return; }
        const form = this.pageForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }

    }
}