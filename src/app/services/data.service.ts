import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()

export class DataService {
    constructor(private http:Http) {

    }

    getData(url:string) {
        return this.http.get(url);
    }
    saveData(url: string, data:object){
        return this.http.post(url, data);
    }
}