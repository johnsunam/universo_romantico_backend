import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RoomModule } from './modules/room/room.module';
import { AmenityModule } from './modules/amenity/amenity.module';
import { LoginComponent } from './login/login.component';
import { ValidationService } from './services/validation.service';
import { ReactiveFormsModule } from '@angular/forms';
import { DataService } from './services/data.service';
import { HeaderComponent } from './layouts/header/header.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { LayoutComponent } from './layouts/layout/layout.component';
import {HttpModule} from '@angular/http';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SidebarComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RoomModule,
    AmenityModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [ValidationService],
  bootstrap: [AppComponent]
})
export class AppModule { }

