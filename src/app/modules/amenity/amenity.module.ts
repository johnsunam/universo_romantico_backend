import { NgModule } from '@angular/core';
//import { AmenityRoutingModule } from './amenity-routing.module';
import { AmenityComponent } from './amenity.component';
import { DataService } from '../../services/data.service';



@NgModule({
  declarations: [
    AmenityComponent,
  ],


  imports: [
   // AmenityRoutingModule
    ],

  providers: [DataService],
  bootstrap: [AmenityComponent]
})
export class AmenityModule { }
