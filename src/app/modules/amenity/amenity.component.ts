import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Response } from '@angular/http';

@Component({
  templateUrl: './amenity.component.html',
  styleUrls: ['./amenity.component.css']
})

export class AmenityComponent {
  url ="http://localhost/universo_romantico_api/api/web/amenities";

  constructor(private dataService:DataService) {

  }

  onSubmit():void {
    let data = { name: 'table' };
    this.dataService.saveData(this.url, data).subscribe((response: Response) => {
      console.log(response);
    });
  }

}
