
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { RoomnumberComponent } from './roomnumber/roomnumber.component';
import { RoomtypeComponent } from './roomtype/roomtype.component';


// const amenityRoutes: Routes = [
//   { path: 'room_no', component: RoomnumberComponent },
//   { path: 'room_type', component: RoomtypeComponent },

// ];

@NgModule({
  // imports: [
  //   RouterModule.forRoot(
  //     amenityRoutes,
  //     {enableTracing: true}
  //   )
  // ],
  exports: [
    RouterModule
  ]
})

export class RoomRoutingModule { };

