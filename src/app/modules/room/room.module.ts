import { NgModule } from '@angular/core';
import { RoomnumberComponent } from './roomnumber/roomnumber.component';
import { RoomtypeComponent } from './roomtype/roomtype.component';


@NgModule({
declarations: [
    RoomnumberComponent,
    RoomtypeComponent
  ],


  providers: [],
  bootstrap: [RoomnumberComponent]
})
export class RoomModule { }
