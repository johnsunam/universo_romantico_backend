import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationService } from '../services/validation.service';
import { Login } from './login';
import { Validators, FormGroup } from '@angular/forms';
@Component({
  templateUrl: './login.component.html',
  styleUrls:['./login.component.css']
})


export class LoginComponent implements OnInit {

    login = new Login();
    submitted = false;

    loginForm: FormGroup;

  formErrors;

  constructor(private validation: ValidationService) {
      console.log(window.location.origin);
    this.validation.formErrors = {
      email: '',
      password:''
    }

    this.validation.pageForm = this.loginForm;

    this.validation.validationMessages = {

      'email': {
        'required':'Name is required.',
      },
      'password': {
        'required':'Password is required.'
      }
    };

    this.formErrors = this.validation.formErrors;

  }

    ngOnInit() {
        console.log(this.login);
        this.validation.buildForm({
            'email': [this.login.email, [
                Validators.required,
                ]
            ],
            'password': [this.login.password, [
                Validators.required,
                ]
            ]
        });


  }

    onSubmit() {
        this.submitted = true
        console.log(this.loginForm);
        //this.login = this.loginForm;
    }


}
