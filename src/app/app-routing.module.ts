import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layouts/layout/layout.component';
import { RoomnumberComponent } from './modules/room/roomnumber/roomnumber.component';
import { RoomtypeComponent } from './modules/room/roomtype/roomtype.component';
import { AmenityComponent } from './modules/amenity/amenity.component';
const appRoutes: Routes = [
    {
        path: '', component: LayoutComponent,

        children: [
            { path: 'room_no', component: RoomnumberComponent },
            { path: 'room_type', component: RoomtypeComponent },
            { path: 'amenity', component: AmenityComponent}
        ]
    },
    {path: 'login', component: LoginComponent},
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true}
        )
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { };
