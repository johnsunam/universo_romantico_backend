import { UniversoRomanticoBackendPage } from './app.po';

describe('universo-romantico-backend App', () => {
  let page: UniversoRomanticoBackendPage;

  beforeEach(() => {
    page = new UniversoRomanticoBackendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
